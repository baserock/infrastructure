# Define required providers
terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.35.0"
    }
  }
}

# Configure the OpenStack Provider
provider "openstack" {
  auth_url   = "https://fra1.citycloud.com:5000"
}

terraform {
  backend "swift" {
    container         = "terraform-state-baserock"
    archive_container = "terraform-state-baserock-archive"
  }
}

locals {
  username               = "cloud"
  image_name             = "Ubuntu 20.04 Focal Fossa 20200423"
  name_prefix            = "bazel-poc"
  flavor_name_frontend   = "1C-1GB-20GB"
  flavor_name_webserver  = "1C-2GB-20GB"
  flavor_name_gbo        = "4C-8GB"
  flavor_name_ostree     = "2C-4GB-20GB"
}


# Create keypairs
resource "openstack_compute_keypair_v2" "pedro-keypair" {
  name       = "pedro-alvarez_latty"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDrfYhQAgqiwtcl37TfBR7N5Fq7ze17Cn4UUbz/Nuby/9qfypUp5Ir2x0P1otbQfozwWBOwmKCFRQMs+fZXFpWsvshNcmaw+rMI8wP1Bx2cqSuPusLPEYbvRbnfGo/E7aj/GvpSKRlBCGF3tORzGAmQsogUUXXcXP7PKIkPB3Jo04K8IeuSoRGd8cGfUWA6dcx9YuZHeJ3o/RzpV8UvU3Ge50mLf05cbrS2LlXgnG2PGbuBX5l87O6u3KUXq5zoafd0AtpSelNcVfAjpwdPokyuR1pXn+3q2w+l7ExmIAjwJV+QJeSSRMRfiHbk/+D3vYUlnqoarB0UrsTb2mY2tAPD"
}
