data "openstack_images_image_v2" "image_id" {
  name        = local.image_name
  most_recent = true
}

# Frontend
data "openstack_compute_flavor_v2" "flavor_frontend" {
  name = local.flavor_name_frontend
}

resource "openstack_networking_port_v2" "frontend_port" {
  name               = "port_1"
  network_id         = "${openstack_networking_network_v2.baserock_network.id}"
  admin_state_up     = "true"

  fixed_ip {
    subnet_id  = "${openstack_networking_subnet_v2.baserock_subnet.id}"
    ip_address = "10.3.0.10"
  }
}

resource "openstack_networking_floatingip_v2" "floatip_frontend" {
  pool = "ext-net"
}

resource "openstack_networking_floatingip_associate_v2" "floatip_associate_frontend" {
  floating_ip = "${openstack_networking_floatingip_v2.floatip_frontend.address}"
  port_id     = "${openstack_networking_port_v2.frontend_port.id}"
}

resource "openstack_compute_instance_v2" "baserock_frontend" {
  name            = "frontend-haproxy"
  image_id        = data.openstack_images_image_v2.image_id.id
  flavor_id       = data.openstack_compute_flavor_v2.flavor_frontend.id
  key_pair        = "${openstack_compute_keypair_v2.pedro-keypair.name}"

  security_groups = [
    "${openstack_networking_secgroup_v2.sg_base.name}",
    "${openstack_networking_secgroup_v2.sg_gitlab_bot.name}",
    "${openstack_networking_secgroup_v2.sg_web_server.name}",
    "${openstack_networking_secgroup_v2.sg_haste_server.name}",
    "${openstack_networking_secgroup_v2.sg_shared_artifact_cache.name}",
  ]
  network {
    port = "${openstack_networking_port_v2.frontend_port.id}"
  }

  lifecycle {
    ignore_changes = [
      # Ignore changes to base image
      image_id,
      # Ignore changes to key_pairs
      key_pair,
    ]
  }
}


# Webserver
data "openstack_compute_flavor_v2" "flavor_webserver" {
  name = local.flavor_name_webserver
}

resource "openstack_networking_port_v2" "webserver_port" {
  name               = "webserver_port"
  network_id         = "${openstack_networking_network_v2.baserock_network.id}"
  admin_state_up     = "true"

  fixed_ip {
    subnet_id  = "${openstack_networking_subnet_v2.baserock_subnet.id}"
    ip_address = "10.3.0.13"
  }
}

resource "openstack_compute_instance_v2" "baserock_webserver" {
  name            = "webserver"
  image_id        = data.openstack_images_image_v2.image_id.id
  flavor_id       = data.openstack_compute_flavor_v2.flavor_webserver.id
  key_pair        = "${openstack_compute_keypair_v2.pedro-keypair.name}"

  security_groups = [
    "${openstack_networking_secgroup_v2.sg_base.name}",
    "${openstack_networking_secgroup_v2.sg_gitlab_bot.name}",
    "${openstack_networking_secgroup_v2.sg_web_server.name}",
    "${openstack_networking_secgroup_v2.sg_haste_server.name}",
  ]
  network {
    port = "${openstack_networking_port_v2.webserver_port.id}"
  }

  lifecycle {
    ignore_changes = [
      # Ignore changes to base image
      image_id,
      # Ignore changes to key_pairs
      key_pair,
    ]
  }
}

resource "openstack_blockstorage_volume_v2" "volume_webserver" {
  name = "webserver-volume"
  size = 150
}

resource "openstack_compute_volume_attach_v2" "volume_attach_webserver" {
  instance_id = "${openstack_compute_instance_v2.baserock_webserver.id}"
  volume_id   = "${openstack_blockstorage_volume_v2.volume_webserver.id}"
  device      = "/dev/vdb"
}

# g.b.o

data "openstack_images_image_v2" "gbo_image_id" {
  name        = "Debian 10 Buster"
  most_recent = true
}

data "openstack_compute_flavor_v2" "flavor_gbo" {
  name = local.flavor_name_gbo
}

resource "openstack_networking_port_v2" "gbo_port" {
  name               = "gbo_port"
  network_id         = "${openstack_networking_network_v2.baserock_network.id}"
  admin_state_up     = "true"

  fixed_ip {
    subnet_id  = "${openstack_networking_subnet_v2.baserock_subnet.id}"
    ip_address = "10.3.0.4"
  }
}


resource "openstack_networking_floatingip_v2" "floatip_gbo" {
  pool = "ext-net"
}

resource "openstack_networking_floatingip_associate_v2" "floatip_associate_gbo" {
  floating_ip = "${openstack_networking_floatingip_v2.floatip_gbo.address}"
  port_id     = "${openstack_networking_port_v2.gbo_port.id}"
}

resource "openstack_compute_instance_v2" "baserock_gbo" {
  name            = "git.baserock.org-debian"
  image_id        = data.openstack_images_image_v2.gbo_image_id.id
  flavor_id       = data.openstack_compute_flavor_v2.flavor_gbo.id
  key_pair        = "${openstack_compute_keypair_v2.pedro-keypair.name}"

  security_groups = [
    "${openstack_networking_secgroup_v2.sg_base.name}",
    "${openstack_networking_secgroup_v2.sg_git_server.name}",
  ]
  network {
    port = "${openstack_networking_port_v2.gbo_port.id}"
  }

  lifecycle {
    ignore_changes = [
      # Ignore changes to base image
      image_id,
      # Ignore changes to key_pairs
      key_pair,
    ]
  }
}

resource "openstack_blockstorage_volume_v2" "volume_gbo" {
  name = "git.baserock.org-srv"
  size = 300
}

resource "openstack_compute_volume_attach_v2" "volume_attach_gbo" {
  instance_id = "${openstack_compute_instance_v2.baserock_gbo.id}"
  volume_id   = "${openstack_blockstorage_volume_v2.volume_gbo.id}"
  device      = "/dev/vdb"
}

# ostree

data "openstack_compute_flavor_v2" "flavor_ostree" {
  name = local.flavor_name_ostree
}

resource "openstack_networking_port_v2" "ostree_port" {
  name               = "ostree_port"
  network_id         = "${openstack_networking_network_v2.baserock_network.id}"
  admin_state_up     = "true"

  fixed_ip {
    subnet_id  = "${openstack_networking_subnet_v2.baserock_subnet.id}"
    ip_address = "10.3.0.12"
  }
}

resource "openstack_compute_instance_v2" "baserock_ostree" {
  name            = "ostree"
  image_id        = data.openstack_images_image_v2.image_id.id
  flavor_id       = data.openstack_compute_flavor_v2.flavor_ostree.id
  key_pair        = "${openstack_compute_keypair_v2.pedro-keypair.name}"

  security_groups = [
    "${openstack_networking_secgroup_v2.sg_base.name}",
    "${openstack_networking_secgroup_v2.sg_web_server.name}",
    "${openstack_networking_secgroup_v2.sg_shared_artifact_cache.name}",
  ]
  network {
    port = "${openstack_networking_port_v2.ostree_port.id}"
  }

  lifecycle {
    ignore_changes = [
      # Ignore changes to base image
      image_id,
      # Ignore changes to key_pairs
      key_pair,
    ]
  }
}

resource "openstack_blockstorage_volume_v2" "volume_ostree" {
  name = "ostree-volume"
  size = 100
}

resource "openstack_compute_volume_attach_v2" "volume_attach_ostree" {
  instance_id = "${openstack_compute_instance_v2.baserock_ostree.id}"
  volume_id   = "${openstack_blockstorage_volume_v2.volume_ostree.id}"
  device      = "/dev/vdb"
}
